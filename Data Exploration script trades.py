# -*- coding: utf-8 -*-
"""
Created on Sat Mar 16 16:31:53 2019
"""

import pandas as pd

# Matplotlib for additional customization
from matplotlib import pyplot as plt

# Seaborn for plotting and styling
import seaborn as sns


PATH = r"C:\Users\939511\Documents\ML_DQ"

df = pd.read_csv(PATH + '\\cum_equities_data.csv')

df = df.sample(frac=0.2) #working with a reduced sample to ease memory load


#Transaction list


numeric = ['PRICE_NOTATION','ADDITIONAL_PRICE_NOTATION','ROUNDED_NOTIONAL_AMOUNT_1','ROUNDED_NOTIONAL_AMOUNT_2','OPTION_STRIKE_PRICE','OPTION_PREMIUM','PRICE_NOTATION2','PRICE_NOTATION3']

for i in numeric:
    if df[i].dtype == 'O' or df[i].dtype == 'S':
        try:
            df[i] = pd.to_numeric(df[i].str.replace(',', '').str.replace('+',''))
        except:
            print(i + ' contains some issues, explore further')

##Date columns
date_cols = ['EXECUTION_TIMESTAMP', 'EFFECTIVE_DATE']
for i in date_cols:
    df[i] = pd.to_datetime(df[i])
    
    
test = df.dtypes

ax = sns.countplot(x=col, data=df)
ax.set_title(f'{col} Categories')
for p in ax.patches:
        ax.annotate('{:.0f}'.format(p.get_height()), (p.get_x()+0.25, p.get_height()+50))
plt.show()


cat_features = df.select_dtypes(include=[object])

for col in cat_features.columns:
    if df[col].nunique() in range(2,20):
        ax = sns.countplot(x=col, data=df)
        ax.set_title(f'{col} Categories')
        for p in ax.patches:
            ax.annotate('{:.0f}'.format(p.get_height()), (p.get_x()+0.25, p.get_height()+50))
        plt.show()
        
        
def plot_distribution( df , var , **kwargs ):
    row = kwargs.get( 'row' , None )
    col = kwargs.get( 'col' , None )
    facet = sns.FacetGrid( df , aspect=4 , row = row , col = col )
    facet.map( sns.kdeplot , var , shade= True )
    facet.set( xlim=( 0 , df[ var ].max() ) )
    facet.add_legend()
    
df_numerical = df.dropna(how='all', axis='columns').select_dtypes(include=['floating'])
for col in df_numerical.columns[1:]:
        plot_distribution(df, var = col)